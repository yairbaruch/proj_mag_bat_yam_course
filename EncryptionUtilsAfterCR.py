#CR : Bad file name! mention this does Caesar's cipher/Shift cipher
from string import ascii_letters, digits


def main():
    # CR: Inconsistent spacing is not readable!
	# CR: Bad integer name
    a =raw_input("Choose encrypt or decrypt!")
    # CR: Redundant input if first input is invalid, Check this only after validation
	# CR: Bad integer name
    s= raw_input("Input desired string")
    if a == "encrypt":
        print(encrpyt(s))
    # CR: Use elif, we perform this check for no reason
    if a == "decrypt":
        print(decrypt(s))
    # CR: No error message in case something went wrong!
# CR: Missing blank lines!
# CR: Function name typo!
# CR: Bad method name, Mention type of encryption
# CR: Parameter shadows built in str
def encrpyt(str):
    # CR: Bad parameter names
    e = ""
    # CR: Could be replaced with list comprehension!
    # "".join([chr(ord(c)+1) for c in str if c in ascii_letters or c in digits])
    for c in str:
        # We only encrypt simple strings with characters and digit, No fancy characters!
        if c in ascii_letters or c in digits:
            new_c = chr(ord(c) + 1)
            e += new_c
    return e
# CR: Bad method name, Mention type of decryption
# CR: Parameter shadows built in str
def decrypt(str):
    # CR: Bad parameter names
    d = ""
    # CR: Could be replaced with list comprehension!
    # "".join([chr(ord(c)-1) for c in str if c in ascii_letters or c in digits])
    for c in str:
        # CR: Off by one error, Try encrypting than decrypting the char 9,Z, or z
        # We only decrypt simple strings with characters and digit, No fancy characters!
        if c in ascii_letters or c in digits:
            new_c = chr(ord(c) - 1)
            d += new_c
    return d


if __name__ == "__main__":
    main()
