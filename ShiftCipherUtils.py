from string import ascii_letters, digits


def main():
    encryption_method = raw_input("Choose encrypt or decrypt!")
    if encryption_method == "encrypt":
        str_to_encrypt = raw_input("Input desired string to encrypt")
        print(shift_by_one_encryption(str_to_encrypt))
    elif encryption_method == "decrypt":
        str_to_decrypt = raw_input("Input desired string to encrypt")
        print(shift_by_one_decrypt(str_to_decrypt))
    else:
        print("Recieved bad encryption method %s, Exiting!" % encryption_method)


def shift_by_one_encryption(str_to_encrypt):
    # We only encrypt simple strings with characters and digit, No fancy charcters!
    return "".join([chr(ord(c) + 1) for c in str_to_encrypt if c in ascii_letters or c in digits])


def shift_by_one_decrypt(str_to_decrypt):
    # We only decrypt simple strings with characters and digit, No fancy charcters!
    "".join([chr(ord(c) - 1) for c in str_to_decrypt if chr(ord(c) - 1) in ascii_letters or c in digits])


if __name__ == "__main__":
    main()
